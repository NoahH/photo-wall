import React, { Component } from 'react';
import "./App.css";

const PHOTO_URL = photoId => `https://picsum.photos/id/${photoId}/200/200`;

const PHOTO_LIST_URL = "https://picsum.photos/list";

class App extends Component {
  constructor(props){
    super(props);
    this.state = {photos: []};
  }

  componentDidMount(){
    fetch(PHOTO_LIST_URL)
    .then(res => res.json())
    .then(data => {
      this.setState({photos: data});
    });
  }

  render() {
    const photos = this.state.photos;
    return (
      <React.Fragment>
        <header className = "header">
          <h1>Photo Wall</h1>
        </header>
        <div className="collage">
          {photos.map(photo => (
            <img
              alt={photo.filename}
              key={photo.id}
              src={PHOTO_URL(photo.id)}
            />
          ))
          }
        </div>

      </React.Fragment>
    );
  }
}

export default App;
